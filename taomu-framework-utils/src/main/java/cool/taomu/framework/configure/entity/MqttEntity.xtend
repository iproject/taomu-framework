package cool.taomu.framework.configure.entity

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class MqttEntity {
	// 是否允许匿名登陆
	boolean anonymous = true;
	String username;
	String password;
	// 服务消息
	String hostname = "0.0.0.0";
	Integer port = 1883;
	boolean useSsl = false;
	SslEntity ssl;
	List<ClusterEntity> cluster;
}
