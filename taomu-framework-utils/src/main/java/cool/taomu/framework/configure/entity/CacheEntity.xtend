package cool.taomu.framework.configure.entity

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class CacheEntity {
    Integer capacity = 100;
    Integer maximumSize = 1000;
    JeidsConfigEntity jedis;
}
