package cool.taomu.framework.configure.entity

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class ObjectPoolEntity {
	Integer maxTotalPerKey = 10;
	Integer maxIdlePerKey = 10;
	Integer minIdlePerKey = 5;
	boolean lifo = false;
}
