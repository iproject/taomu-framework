package cool.taomu.framework.crypto

import org.apache.commons.codec.digest.DigestUtils
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Md5 implements ICrypto {

	byte[] data;

	new(byte[] data) {
		this.data = data;
	}

	override decode() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override encode() {
		return DigestUtils.md5Hex(data).bytes;
	}

	override setData(byte[] src) {
		this.data = src;
		return this;
	}

}
