/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.crypto

import cool.taomu.framework.utils.crypto.PgpUtils
import java.security.Security
import org.eclipse.xtend.lib.annotations.Accessors
import org.bouncycastle.jce.provider.BouncyCastleProvider

@Accessors
class SimplePgp implements ICrypto {
	ICrypto c = null;
	String key;
	String password;
	byte[] src;

	new(byte[] src, String publicKey) {
		Security.addProvider(new BouncyCastleProvider());
		this.src = src;
		this.key = publicKey;
	}

	new(byte[] src, String privateKey, String password) {
		Security.addProvider(new BouncyCastleProvider());
		this.src = src;
		this.key = privateKey;
		this.password = password;

	}

	new(ICrypto c, String publicKey) {
		Security.addProvider(new BouncyCastleProvider());
		this.c = c;
		this.key = publicKey;
	}

	new(ICrypto c, String privateKey, String passwod) {
		Security.addProvider(new BouncyCastleProvider());
		this.c = c;
		this.key = privateKey;
		this.password = password;
	}

	override decode() {
		if (c !== null) {
			var bytes = PgpUtils.decrypt(this.c.data, this.key, this.password);
			this.c.data = bytes;
			return c.decode();
		} else {
			return PgpUtils.decrypt(this.src, this.key, this.password);
		}
	}

	override encode() {
		if (c !== null) {
			return PgpUtils.encrypt(this.c.encode(), this.key, true);
		} else {
			return PgpUtils.encrypt(this.src, this.key, true);
		}
	}

	override getData() {
		return this.src;
	}

	override setData(byte[] src) {
		this.src = src;
		return this;
	}

}
