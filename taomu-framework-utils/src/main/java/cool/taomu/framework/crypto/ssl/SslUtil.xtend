/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.crypto.ssl

import java.io.FileInputStream
import java.security.KeyStore
import java.security.SecureRandom
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory
import org.jsoup.Jsoup

class SslUtil {
	KeyStore keyStore;

	new(String keyStoreType) {
		keyStore = KeyStore.getInstance(keyStoreType === null ? "JKS" : keyStoreType);
	}

	def trustManageFactory() {
		var tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(keyStore);
		return tmf;
	}

	def trustManageFactory(String sslKeyFilePath, String sslStorePwd) {
		try {
			keyStore.load(new FileInputStream(sslKeyFilePath), sslStorePwd.toCharArray());
			val tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			tmf.init(keyStore);
			return tmf;
		} catch (Exception ex) {
			return null;
		}
	}

	def keyManageFactory(String sslKeyFilePath, String sslManagerPwd, String sslStorePwd) {
		try {
			keyStore.load(new FileInputStream(sslKeyFilePath), sslStorePwd.toCharArray());
			val kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(keyStore, sslManagerPwd.toCharArray());
			return kmf;
		} catch (Exception ex) {
			return null;
		}
	}

	def SSLSocketFactory getSslSocketFactory() {
		var sslContext = SSLContext.getInstance("TLS");
		var trustManagers = #[]
		sslContext.init(null,trustManagers,new SecureRandom());
		return null;
	}

	def test() {
		var a = Jsoup.connect("");
	}
}
