package cool.taomu.framework.utils.json

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException

class JsonUtils {
    static Gson gson = new Gson();

    private new() {
    }

    def static <T> toJson(T obj) {
        return gson.toJson(obj);
    }

    def static <T> T toObject(String json, Class<?> zlass) throws JsonSyntaxException{
        return gson.fromJson(json, zlass) as T;
    }
}
