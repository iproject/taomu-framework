package cool.taomu.framework.utils.validator

import jakarta.validation.Validation
import org.hibernate.validator.HibernateValidator
import org.apache.commons.collections4.CollectionUtils

/**
 *  注解 | 类型 | 说明
 *  ---|---|---
 *  @NotNull | 任意类型 | 验证注解的元素值不是null
 *  @Null | 任意类型 | 验证注解的元素值是null
 *  @Min(value=值) | BigDecimal，BigInteger, byte,short, int, long，等任何Number或CharSequence（存储的是数字）子类型 | 验证注解的元素值大于等于@Min指定的value值
 *  @Max(value=值) | 字符串、Collection、Map、数组等 | 验证注解的元素值的在min和max（包含）指定区间之内，如字符长度、集合大小
 *  @Size(min=下限, max=上限) | 和@Min要求一样 | 验证注解的元素值小于等于@Max指定的value值
 *  @NotBlank | CharSequence子类型 | 验证注解的元素值不为空（不为null、不为空字符串、trim之后不为空字符串）
 *  @Length(min=下限, max=上限) | CharSequence子类型 | 验证注解的元素值长度在min和max区间内
 *  @NotEmpty  | CharSequence子类型、Collection、Map、数组 | 验证注解的元素值不为null且不为空（字符串长度不为0、集合大小不为0）
 *  @Range(min=最小值, max=最大值) | BigDecimal,BigInteger,CharSequence, byte, short, int, long等原子类型和包装类型 | 验证注解的元素值在最小值和最大值之间
 *  @Email(regexp=正则表达式,flag=标志的模式) | CharSequence子类型（如String）| 验证注解的元素值是Email，也可以通过regexp和flag指定自定义的email格式
 *  @Pattern(regexp=正则表达式,flag=标志的模式) | String，任何CharSequence的子类型 | 验证注解的元素值与指定的正则表达式匹配
 *  @Valid | 任何非原子类型 |指定递归验证关联的对象如用户对象中有个地址对象属性，如果想在验证用户对象时一起验证地址对象的话，在地址对象上加@Valid注解即可级联验证
 * @author rcmu
 *
 */
class ValidatorUtils {
	val static validator = Validation.byProvider(HibernateValidator).configure().failFast(false).
		buildValidatorFactory().validator;

	def static <T> validate(T obj, Class<?> ... classes) {
		var constrainViolations = validator.validate(obj, classes);
		if (CollectionUtils.isNotEmpty(constrainViolations)) {
			var msgs = constrainViolations.map[return it.message]
			return msgs;
		}
		return null;
	}
}
