package cool.taomu.framework.utils.runtime

import com.sun.management.OperatingSystemMXBean
import cool.taomu.framework.utils.ByteToSize
import java.lang.management.ManagementFactory
import java.lang.management.MemoryMXBean
import org.apache.commons.io.IOUtils
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class RuntimeInfo {
	String vmInfo
	String javaInfo
	String jvmMemUsed
	String jvmMemTotal
	String jvmMemFree
	String jvmMemMax
	String heapMemUsed;
	String heapMemMax;
	String heapMemCommitted;
	String osInfo
	String memUsed
	String memFree
	String memTotal
	int threadTotalNum
	String pid;
	long loadedClassCount;
	long totalloadedClassCount;
	long unloadedClassCount;
	int cpuTemp;
	double sysCpuload;
	double proCpuload;
	int cpuCore;

	new() {
		var long vmFree = 0;
		var long vmUse = 0;
		var long vmTotal = 0;
		var long vmMax = 0;
		var Runtime rt = Runtime.getRuntime();
		vmTotal = rt.totalMemory();
		vmFree = rt.freeMemory();
		vmMax = rt.maxMemory();
		vmUse = vmTotal - vmFree;
		this.jvmMemFree = ByteToSize.sizeDescription(String.valueOf(vmFree));
		this.jvmMemUsed = ByteToSize.sizeDescription(String.valueOf(vmUse));
		this.jvmMemTotal = ByteToSize.sizeDescription(String.valueOf(vmTotal));
		this.jvmMemMax = ByteToSize.sizeDescription(String.valueOf(vmMax));
		var osmxb = ManagementFactory.getOperatingSystemMXBean() as OperatingSystemMXBean;
		var String os = String.format("%s arch %s version %s", osmxb.name, osmxb.arch, osmxb.version);
		this.sysCpuload = osmxb.systemCpuLoad;
		this.proCpuload = osmxb.processCpuLoad;
		this.cpuCore = osmxb.availableProcessors;
		var long physicalFree = osmxb.getFreePhysicalMemorySize();
		var long physicalTotal = osmxb.getTotalPhysicalMemorySize();
		var long physicalUse = physicalTotal - physicalFree;
		this.osInfo = os;
		if (osInfo.contains("Linux")) {
			var temp = Runtime.runtime.exec("cat /sys/class/thermal/thermal_zone0/temp").inputStream;
			var String temp1 = IOUtils.toString(temp, "UTF-8");
			if (temp1.equals("")) {
				temp = Runtime.runtime.exec("cat /sys/class/hwmon/hwmon0/in0_input").inputStream;
			}
			var temperature = IOUtils.toString(temp, "UTF-8").trim;
			if (!temperature.equals(""))
				this.cpuTemp = Integer.parseInt(temperature) / 1000
		}
		this.memFree = ByteToSize.sizeDescription(String.valueOf(physicalFree));
		this.memUsed = ByteToSize.sizeDescription(String.valueOf(physicalUse));
		this.memTotal = ByteToSize.sizeDescription(String.valueOf(physicalTotal));
		// 获得线程总数
		var ThreadGroup parentThread;
		var int totalThread = 0;
		for (parentThread = Thread.currentThread().getThreadGroup(); parentThread.getParent() !==
			null; parentThread = parentThread.getParent()) {
			totalThread = parentThread.activeCount();
		}
		this.threadTotalNum = totalThread;
		this.pid = ManagementFactory.getRuntimeMXBean().getName().split("@").get(0);
		var runtime = ManagementFactory.runtimeMXBean;
		this.vmInfo = String.format("%s version %s", runtime.vmName, runtime.vmVersion);
		this.javaInfo = String.format("%s java version %s", runtime.vmVendor, System.getProperty("java.version"));
		var cl = ManagementFactory.getClassLoadingMXBean();
		this.loadedClassCount = cl.loadedClassCount;
		this.totalloadedClassCount = cl.totalLoadedClassCount;
		this.unloadedClassCount = cl.unloadedClassCount;
		var MemoryMXBean mem = ManagementFactory.getMemoryMXBean();
		this.heapMemUsed = ByteToSize.sizeDescription(String.valueOf(mem.getHeapMemoryUsage().getUsed()));
		this.heapMemMax = ByteToSize.sizeDescription(String.valueOf(mem.getHeapMemoryUsage().getMax()));
		this.heapMemCommitted = ByteToSize.sizeDescription(String.valueOf(mem.getHeapMemoryUsage().getCommitted()));
	}

	def static void main(String[] args) {
		var a = new RuntimeInfo();
		println(a)
	}
}
