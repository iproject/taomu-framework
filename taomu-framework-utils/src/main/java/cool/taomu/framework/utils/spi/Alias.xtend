package cool.taomu.framework.utils.spi

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.Target

@Documented
@Retention(RUNTIME)
@Target(#[TYPE])
annotation Alias {
	String value ;
}