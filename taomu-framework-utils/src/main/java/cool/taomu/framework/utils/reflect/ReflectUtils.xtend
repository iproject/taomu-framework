/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.utils.reflect

import org.slf4j.LoggerFactory

class ReflectUtils {
	static val LOG = LoggerFactory.getLogger(ReflectUtils);

	def static getReflectFields(Class<?> zlass) {
		zlass.declaredFields
	}

	def static setReflectFields(Class<?> zlass, String name, Object instance, Object value) {
		var field = zlass.getDeclaredField(name);
		field.accessible = true;
		field.set(instance, value);
	}

	def static getMethod(Class<?> zlass, String name, Class<?> ... parameterTypes) {
		var method = zlass.getMethod(name, parameterTypes);
		method.accessible = true;
		return method;
	}

	def static getReflectField(Class<?> zlass, String name) {
		var field = zlass.getDeclaredField(name);
		field.accessible = true;
		return field;
	}

	def static Object newInstance(Class<?> zlass, Object ... constructorArgs) {
		var Object p = null;
		if (constructorArgs !== null && constructorArgs.size > 0) {
			var argsClass = constructorArgs.map[return it.class]
			try {
				p = zlass.getDeclaredConstructor(argsClass).newInstance(constructorArgs);
			} catch (NoSuchMethodException e) {
				LOG.info(e.message);
				argsClass = constructorArgs.map[return it.class.superclass]
				p = zlass.getDeclaredConstructor(argsClass).newInstance(constructorArgs);
			}
		} else {
			p = zlass.newInstance();
		}
		return p;
	}
}
