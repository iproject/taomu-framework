/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.cache

import cool.taomu.framework.crypto.Base64
import java.io.Serializable
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.LinkedBlockingQueue
import org.apache.commons.lang3.SerializationUtils
import org.slf4j.LoggerFactory

class QueueCache<T extends Serializable> implements ICache<String,T>{
	val static LOG = LoggerFactory.getLogger(QueueCache);
	val cache = new ConcurrentHashMap<String, BlockingQueue<T>>;
	val jedis = JedisUtils.instance.jedis;
	var static QueueCache<?> instance;

	private new() {
	}

	def static <T extends Serializable> getInstance() {
		if (instance === null) {
			instance = new QueueCache<T>();
		}
		return instance;
	}

	def T poll(String key) {
		if (jedis !== null) {
			LOG.info("从jeids中获取数据,key:{}", key);
			var result = jedis.rpop(key);
			if (!result.isNullOrEmpty) {
				var base64 = new Base64(result.bytes).decode();
				if (!base64.isNullOrEmpty) {
					return SerializationUtils.deserialize(base64);
				}
			}
		} else {
			LOG.info("从本地缓存中获取数据,key:{}", key);
			if (cache.containsKey(key)) {
				return cache.get(key).poll();
			}
		}
		return null;
	}

	override put(String key, T value) {
		if (jedis !== null) {
			if (value !== null) {
				LOG.info("将数据压入jeids中,key:{}", key);
				var ser = SerializationUtils.serialize(value);
				jedis.lpush(key, new String(new Base64(ser).encode, "UTF-8"));
			}
		} else {
			LOG.info("将数据压入本地缓存中,key:{}", key);
			if (cache.containsKey(key)) {
				cache.get(key).put(value);
			} else {
				cache.put(key, new LinkedBlockingQueue<T>())
				cache.get(key).put(value);
			}
		}
	}
	
	override get(String key) {
		return this.poll(key);
	}
	
	override remove(String key) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
	override remove(String key, T value) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
	override refresh(String key) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}
