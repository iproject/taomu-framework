/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.cache

import com.google.common.base.Optional
import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import cool.taomu.framework.configure.ConfigureManage
import cool.taomu.framework.crypto.Base64
import java.io.Serializable
import org.apache.commons.lang3.SerializationUtils
import org.slf4j.LoggerFactory

class KeyValueCache implements ICache<String, Serializable> {
	val static LOG = LoggerFactory.getLogger(KeyValueCache);
	var static LoadingCache<String, Optional<Serializable>> cache;
	val static KeyValueCache instance = new KeyValueCache();
	val jedis = JedisUtils.instance.jedis;
	val config = ConfigureManage.loadConfig;

	private new() {
		cache = CacheBuilder.newBuilder().concurrencyLevel(Runtime.getRuntime().availableProcessors()).initialCapacity(
			config.cache.capacity).maximumSize(config.cache.maximumSize).build(
			new CacheLoader<String, Optional<Serializable>>() {
				override load(String key) throws Exception {
					if (jedis !== null) {
						LOG.info("从redis中加载数据")
						var result = jedis.get(key);
						var base64 = new Base64(result.bytes).decode;
						return Optional.of(SerializationUtils.deserialize(base64));
					}
					return Optional.absent;
				}
			});
	}

	def static getInstance() {
		return instance;
	}

	def store(String key, Serializable value) {
		cache.put(key, Optional.of(value));
		if (jedis !== null) {
			var result = SerializationUtils.serialize(value);
			var base64 = new Base64(result).encode;
			jedis.set(key, new String(base64));
		}
	}

	override get(String key) {
		var result = cache.get(key);
		if (result.present) {
			return result.get();
		}
		return null;
	}

	override refresh(String key) {
		cache.refresh(key);
	}

	override remove(String key) {
		cache.invalidate(key);
		if (jedis !== null) {
			jedis.del(key);
		}
	}

	def static void main(String[] args) {
		KeyValueCache.instance.store("aa", "bb");
		System.out.println(KeyValueCache.instance.get("aa"));
		KeyValueCache.instance.refresh("aa");
		KeyValueCache.instance.refresh("aa");
		KeyValueCache.instance.refresh("aa");
	// FigCache.instance.remove("aa");
	// System.out.println(FigCache.instance.get("aa"));
	}

	override put(String key, Serializable value) {
		this.store(key, value);
	}
	
	override remove(String key, Serializable value) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

}
