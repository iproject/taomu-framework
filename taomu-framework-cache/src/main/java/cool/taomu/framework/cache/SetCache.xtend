/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.cache

import cool.taomu.framework.crypto.Base64
import java.io.Serializable
import java.util.HashSet
import java.util.Set
import java.util.concurrent.ConcurrentHashMap
import org.apache.commons.lang3.SerializationUtils
import org.slf4j.LoggerFactory

class SetCache<T extends Serializable> implements ICache<String, Set<T>> {
	val static LOG = LoggerFactory.getLogger(SetCache);
	val cache = new ConcurrentHashMap<String, Set<T>>;
	val jedis = JedisUtils.instance.jedis;

	var static Object instance;

	private new() {
	}

	def static <T extends Serializable> SetCache<T> getInstance() {
		if (instance === null) {
			instance = new SetCache<T>();
		}
		return instance as SetCache<T>;
	}

	override Set<T> get(String key) {
		if (jedis !== null) {
			LOG.info("从jeids中获取数据,key:{}", key);
			var results = jedis.smembers(key);
			if (!results.isNullOrEmpty) {
				var hashSet = new HashSet<T>();
				for (result : results) {
					var base64 = new Base64(result.bytes).decode();
					if (!base64.isNullOrEmpty) {
						hashSet.add(SerializationUtils.deserialize(base64));
					}
				}
				return hashSet;
			}
		} else {
			LOG.info("从本地缓存中获取数据,key:{}", key);
			if (cache.containsKey(key)) {
				return cache.get(key);
			}
		}
		return null;
	}

	def add(String key, T value) {
		if (jedis !== null) {
			if (value !== null) {
				LOG.info("将数据压入jeids中,key:{}", key);
				var ser = SerializationUtils.serialize(value);
				jedis.sadd(key, new String(new Base64(ser).encode, "UTF-8"));
			}
		} else {
			LOG.info("将数据压入本地缓存中,key:{}", key);
			if (cache.containsKey(key)) {
				cache.get(key).add(value);
			} else {
				LOG.info("创建一个HashSet");
				cache.put(key, new HashSet<T>);
				cache.get(key).add(value);
			}
		}
	}

	def remove(String key, T value) {
		if (jedis !== null) {
			if (value !== null) {
				LOG.info("将数据{}从jeids中移除", key);
				var ser = SerializationUtils.serialize(value);
				jedis.srem(key, new String(new Base64(ser).encode, "UTF-8"));
			}
		} else {
			LOG.info("将数据{}从本地缓存中移除", key);
			if (cache.containsKey(key)) {
				cache.get(key).remove(value);
			}
		}
	}

	def static void main(String[] args) {
		var s = new SetCache<String>();
		s.add("key", "aaa")
		s.add("key", "bbb")
		println(s.get("key"));
	}

	override remove(String key) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override refresh(String key) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override put(String key, Set<T> value) {
		value.forEach[this.add(key, it)];
	}

	override remove(String key, Set<T> value) {
		value.forEach[this.remove(key, it)];
	}

}
