package cool.taomu.framework.service.test

import cool.taomu.framework.service.rpc.Gateway
import cool.taomu.framework.service.rpc.Gateway.Iface
import cool.taomu.framework.service.rpc.TRpcServer
import cool.taomu.framework.utils.spi.ServiceLoader

class TServerTest {
	def static void main(String[] args) {
		var client = ServiceLoader.load(Gateway.Iface);
		var processor = new Gateway.Processor<Iface>(client.first);
		TRpcServer.server("0.0.0.0", 8899, 2, 4, processor);
	}
}
