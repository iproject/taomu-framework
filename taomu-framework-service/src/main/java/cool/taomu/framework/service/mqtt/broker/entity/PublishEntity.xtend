/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.entity

import io.netty.handler.codec.mqtt.MqttQoS
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class PublishEntity {
	MqttQoS qos;	
	String topicName;
	Integer messageId;
	byte[] payload;
	Boolean dup;
	new(MqttQoS qos,String topicName,Integer messageId,byte[] payload,Boolean dup){
		this.qos = qos;
		this.topicName= topicName;
		this.messageId = messageId;
		this.payload = payload;
		this.dup = dup;
	}
}
