/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.response

import cool.taomu.framework.service.mqtt.broker.entity.PubAckEntity
import cool.taomu.framework.service.mqtt.broker.inter.IResponse
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessageIdVariableHeader
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttPubAckMessage
import io.netty.handler.codec.mqtt.MqttQoS

class PubAckResponse implements IResponse<PubAckEntity> {

	override response(PubAckEntity entity) {
		var header = new MqttFixedHeader(MqttMessageType.PUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0);
		var varHeader = MqttMessageIdVariableHeader.from(entity.messageId);
		return new MqttPubAckMessage(header, varHeader);
	}

}
