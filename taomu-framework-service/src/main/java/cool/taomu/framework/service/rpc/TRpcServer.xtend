package cool.taomu.framework.service.rpc

import java.net.InetSocketAddress
import org.apache.thrift.TProcessor
import org.apache.thrift.TProcessorFactory
import org.apache.thrift.protocol.TCompactProtocol
import org.apache.thrift.server.THsHaServer
import org.apache.thrift.transport.TNonblockingServerSocket
import org.apache.thrift.transport.layered.TFramedTransport

class TRpcServer {
	def static server(String ip,int port, int minWorkerThreads, int maxWorkerThreads, TProcessor processor) {
		var socket = new TNonblockingServerSocket(new InetSocketAddress(ip,port));
		var arg = new THsHaServer.Args(socket).minWorkerThreads(minWorkerThreads).maxWorkerThreads(maxWorkerThreads);
		arg.protocolFactory(new TCompactProtocol.Factory());
		arg.transportFactory(new TFramedTransport.Factory());
		arg.processorFactory(new TProcessorFactory(processor));
		var server = new THsHaServer(arg);
		server.serve();
	}
}
