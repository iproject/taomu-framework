/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.request

import cool.taomu.framework.cache.SetCache
import cool.taomu.framework.service.mqtt.broker.entity.MessageEntity
import cool.taomu.framework.service.mqtt.broker.entity.PubCompEntity
import cool.taomu.framework.service.mqtt.broker.impl.response.PubCompResponse
import cool.taomu.framework.service.mqtt.broker.inter.IRequest
import cool.taomu.framework.service.mqtt.broker.inter.IResponse
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.Alias
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttMessage
import org.slf4j.LoggerFactory

import static extension cool.taomu.framework.service.utils.CommonUtils.*

/**
 * PUBCOMP  |   7   |   两个方向都允许     |   QoS 2 消息发布完成(保证交付第三步)
 */
@Alias(value="PUBCOMP")
class PubCompRequest implements IRequest {
	val LOG = LoggerFactory.getLogger(PubCompRequest);

	SetCache<MessageEntity> cache = SetCache.instance;

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		var clientId = CommonUtils.getClientId(ctx.channel);
		LOG.info("执行了MQTT PubComp 命令 : " + clientId);
		var msgId = CommonUtils.getMessageId(mqttMessage);
		//增加一个给发布者发一个Pubcomp命令，和删除消息的方法根据clientId
		var qos2 = cache.get(clientId.qos2Message());
		for (q : qos2) {
			var msg = q as MessageEntity;
			var resp = new PubCompResponse().response(new PubCompEntity(msgId));
			msg.senderChannel.writeAndFlush(resp);
			cache.remove(clientId.qos2Message,msg);
		}
		return null;
	}

}
