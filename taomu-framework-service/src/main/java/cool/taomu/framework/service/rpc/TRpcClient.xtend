package cool.taomu.framework.service.rpc

import org.apache.thrift.protocol.TCompactProtocol
import org.apache.thrift.protocol.TProtocol
import org.apache.thrift.transport.TSocket
import org.apache.thrift.transport.layered.TFramedTransport

abstract class TRpcClient {
	abstract def <T> void call(TProtocol protocol);

	def static <T> void client(String host, int port, int maxLength, TRpcClient client) {
		try(var tTransport = new TFramedTransport(new TSocket(host,port),maxLength)) {
			var protocol = new TCompactProtocol(tTransport);
			tTransport.open();
			client.call(protocol);
		}
	}
}
