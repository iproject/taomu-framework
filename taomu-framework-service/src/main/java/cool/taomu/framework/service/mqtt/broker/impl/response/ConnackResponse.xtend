/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.response

import cool.taomu.framework.service.mqtt.broker.entity.ConnackEntity
import cool.taomu.framework.service.mqtt.broker.inter.IResponse
import io.netty.handler.codec.mqtt.MqttConnAckMessage
import io.netty.handler.codec.mqtt.MqttConnAckVariableHeader
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttQoS
import org.slf4j.LoggerFactory

/**
 * CONNACK  V   |   2   |   服务端到客户端     |   连接报文确认
 */
class ConnackResponse implements IResponse<ConnackEntity> {
	val LOG = LoggerFactory.getLogger(ConnackResponse);

	override response(ConnackEntity entity) {
		LOG.info("执行了MQTT Connack 命令")
		var header = new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE, false, 0);
		var varHeader = new MqttConnAckVariableHeader(entity.code, entity.sessionPresent);
		return new MqttConnAckMessage(header, varHeader);
	}

}
