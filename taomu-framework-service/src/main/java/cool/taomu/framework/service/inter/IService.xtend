package cool.taomu.framework.service.inter

import java.lang.reflect.Method

interface IService {
	def void service(Class<?> zlass, Method method);
}
