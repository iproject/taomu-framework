/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.entity

import java.util.concurrent.atomic.AtomicInteger
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.EqualsHashCode
import org.eclipse.xtend.lib.annotations.ToString
import io.netty.channel.ChannelHandlerContext
import java.io.Serializable

@Accessors
@ToString
@EqualsHashCode
class ClientSessionEntity implements Serializable{
	String clientId;
	// mqttv3 为false时，则 Broker 会为终端存储消息
	boolean cleanStatus;
	transient ChannelHandlerContext ctx;
	transient AtomicInteger messageIdCounter = new AtomicInteger(1);

	def int generateMessageId() {
		var messageId = messageIdCounter.getAndIncrement();
		messageId = Math.abs(messageId % 0xFFFF);
		if (messageId == 0) {
			return generateMessageId();
		}
		return messageId;
	}
}
