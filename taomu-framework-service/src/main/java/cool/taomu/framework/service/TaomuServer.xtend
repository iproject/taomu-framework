package cool.taomu.framework.service

import cool.taomu.framework.service.mqtt.broker.MQTTBroker

class TaomuServer {
	def static void main(String[] args) {
		var mqtt = new MQTTBroker;
		mqtt.startTcpServer();
	}
}
