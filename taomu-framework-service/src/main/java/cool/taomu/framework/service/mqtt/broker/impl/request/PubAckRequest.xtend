/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.request

import cool.taomu.framework.service.mqtt.broker.inter.IRequest
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.Alias
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttMessage
import org.slf4j.LoggerFactory

/**
 * PUBACK		|	4 	|	两个方向都允许	|	QoS 1 消息发布收到确认
 */
@Alias(value="PUBACK")
class PubAckRequest implements IRequest {
	
	val LOG = LoggerFactory.getLogger(PubAckRequest);


	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		var clientId = CommonUtils.getClientId(ctx.channel);
		if(clientId === null){
		  clientId = CommonUtils.getClientId(ctx.channel);
		}
		LOG.info("执行了MQTT PubAck 命令 " + clientId);
		return null;
	}

}
