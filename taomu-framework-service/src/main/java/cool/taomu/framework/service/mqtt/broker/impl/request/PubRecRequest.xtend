/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.service.mqtt.broker.impl.request

import cool.taomu.framework.cache.SetCache
import cool.taomu.framework.service.mqtt.broker.entity.MessageEntity
import cool.taomu.framework.service.mqtt.broker.entity.PubRecEntity
import cool.taomu.framework.service.mqtt.broker.impl.response.PubRecResponse
import cool.taomu.framework.service.mqtt.broker.inter.IRequest
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.Alias
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttMessage
import io.netty.handler.codec.mqtt.MqttMessageIdVariableHeader
import org.slf4j.LoggerFactory

import static extension cool.taomu.framework.service.utils.CommonUtils.*

/**
 * PUBREC |	5 	|	两个方向都允许 	|	发布收到(保证交付第一步)
 */
@Alias(value="PUBREC")
class PubRecRequest implements IRequest {

	val LOG = LoggerFactory.getLogger(PubRecRequest);


	SetCache<MessageEntity> cache = SetCache.instance;

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		var clientId = CommonUtils.getClientId(ctx.channel())
		LOG.info("执行了MQTT PubRec 命令 : " + clientId);
		var idVariableHeader = mqttMessage.variableHeader() as MqttMessageIdVariableHeader;
		var msgId = idVariableHeader.messageId();
		// 回给发送这个一个REC命令
		var qos2 = cache.get(clientId.qos2Message);
		for (q : qos2) {
			var msg = q as MessageEntity;
			var entity = new PubRecResponse().response(new PubRecEntity(msgId));
			msg.senderChannel.writeAndFlush(entity);
			cache.remove(clientId.qos2Message, msg)
			msg.senderChannel = ctx.channel;
			var senderId = msg.senderId;
			msg.senderId = clientId;
			cache.add(senderId.qos2Message, msg);
		}
		return null // #[pubrel.response(new PubRelEntity(id.messageId()))];
	}
}
