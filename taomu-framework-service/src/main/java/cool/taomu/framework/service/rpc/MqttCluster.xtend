package cool.taomu.framework.service.rpc

import com.google.gson.Gson
import cool.taomu.framework.cache.KeyValueCache
import cool.taomu.framework.service.mqtt.broker.entity.MessageEntity
import cool.taomu.framework.service.mqtt.broker.impl.PublishObservable
import cool.taomu.framework.service.mqtt.broker.inter.IPublishObserver.Type
import cool.taomu.framework.utils.asm.BytecodeUtils
import cool.taomu.framework.utils.reflect.ReflectUtils
import cool.taomu.framework.utils.runtime.RuntimeInfo
import cool.taomu.framework.utils.spi.ServiceLoader
import org.apache.thrift.TException
import cool.taomu.framework.utils.reflect.CallSiteUtils

class MqttCluster implements Gateway.Iface {

	KeyValueCache cache = KeyValueCache.instance;

	override loadInfo() throws TException {
		return new Response(200, new Gson().toJson(new RuntimeInfo()));
	}

	override publish(String source, String data) throws TException {
		cache.store(source, new Gson().fromJson(data, MessageEntity));
		// 推送用户订阅
		PublishObservable.instance.start(source, Type.MESSAGE);
		return new Response(200, "");
	}

	override refresh(String key) throws TException {
		cache.refresh(key);
		return new Response(200, "");
	}

	/**
	 * serviceName inteface
	 * callSite impl method 
	 * arg args
	 */
	override call(String serviceName, String callSite, String arg) throws TException {
		try {
			var zlass = Thread.currentThread.contextClassLoader.loadClass(serviceName);
			if (zlass.getAnnotation(FunctionalInterface) !== null) {
				var csu = new CallSiteUtils(zlass);
				var sl = ServiceLoader.load(zlass);
				var zlassImpl = sl.getZlass(callSite);
				var result = csu.bind(zlassImpl, arg);
				return new Response(200, new Gson().toJson(result));
			}
			return new Response(404, "");
		} catch (Exception e) {
			e.printStackTrace
			return new Response(404, "");
		}
	}

}
