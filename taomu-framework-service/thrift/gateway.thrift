namespace java cool.taomu.framework.service.rpc

struct Response{
	1:i32 status,
	2:string results
}
service Gateway{
  Response publish(1:string source,2:string data)
  Response refresh(1:string key)
  Response loadInfo()
  Response call(1:required string serviceName,2:required string callSite,3:string arg);
}
