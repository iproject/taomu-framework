package cool.taomu

import cool.taomu.thread.NetworkThreadServer
import cool.taomu.thread.TaomuThreadPoolFactory
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.CommandLineParser
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options

class Taomu {
	def static void main(String[] args) {
		var Options options = new Options();
		options.addOption("h", "help", false, "Print this usage information");
		options.addOption("v", "verbose", false, "Print out VERBOSE information");
		options.addOption("q","queue", true, "queue size");
		var CommandLineParser parser = new DefaultParser();
		var CommandLine commandLine = parser.parse(options, args);
		if (commandLine.hasOption('q')) {
			var qSize = Integer.parseInt(commandLine.getOptionValue('q'));
			TaomuThreadPoolFactory.init(qSize);
		}
		NetworkThreadServer.start();
	}
}
