package cool.taomu.thread

import cool.taomu.utils.concurrent.Runnable
import java.nio.ByteBuffer
import org.apache.commons.lang3.SerializationUtils
import org.apache.thrift.TException
import org.apache.thrift.TProcessor
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.server.TServer
import org.apache.thrift.server.TSimpleServer
import org.apache.thrift.transport.TServerSocket

class NetworkThreadServer implements NetworkThread.Iface {

	override asyncThread(ByteBuffer serializableByte) throws TException {
		var ser = serializableByte.array;
		var runnable = SerializationUtils.deserialize(ser) as Runnable;
		var a = TaomuThreadPoolFactory.getThreadPool;
		a.execute(runnable);

	}

	override syncThread(ByteBuffer serializableByte) throws TException {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	def static start() {
		var TServerSocket serverTransport = new TServerSocket(9090);
        var TServer.Args tArgs = new TServer.Args(serverTransport);
        tArgs.protocolFactory(new TBinaryProtocol.Factory());
        var TProcessor tprocessor =new NetworkThread.Processor(new NetworkThreadServer());
        tArgs.processor(tprocessor);
        var TServer server = new TSimpleServer(tArgs);
        System.out.println("UserService TSimpleServer start ....");
        server.serve();
	}
}
