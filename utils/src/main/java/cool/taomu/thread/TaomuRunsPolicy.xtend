package cool.taomu.thread

import cool.taomu.utils.concurrent.Runnable
import java.nio.ByteBuffer
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy
import org.apache.commons.lang3.SerializationUtils
import org.apache.thrift.TException
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.protocol.TProtocol
import org.apache.thrift.transport.TSocket
import org.apache.thrift.transport.TTransport
import org.slf4j.LoggerFactory

class TaomuRunsPolicy extends CallerRunsPolicy {
	val static LOG = LoggerFactory.getLogger(TaomuRunsPolicy);

	override rejectedExecution(java.lang.Runnable r, ThreadPoolExecutor executor) {
		LOG.info("当前被拒绝任务为:{}",r.toString());
		if (r instanceof Runnable) {
			var ser = SerializationUtils.serialize(r);
			try {
				var TTransport transport = new TSocket("localhost", 9090);
				var TProtocol protocol = new TBinaryProtocol(transport);
				var NetworkThread.Client client = new NetworkThread.Client(protocol);
				transport.open();
				client.asyncThread(ByteBuffer.wrap(ser));
			} catch (TException e) {
				LOG.info("网络异常：",e);
				super.rejectedExecution(r, executor);
			}
		} else {
			super.rejectedExecution(r, executor);
		}
	}

}
