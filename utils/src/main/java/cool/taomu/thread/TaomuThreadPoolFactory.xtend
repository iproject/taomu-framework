package cool.taomu.thread

import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import org.slf4j.LoggerFactory

class TaomuThreadPoolFactory {
	val static LOG = LoggerFactory.getLogger(TaomuThreadPoolFactory);
	val static coreSize = Runtime.runtime.availableProcessors;
	var static ThreadPoolExecutor threadPool = null;

	def static getThreadPool() {
		return threadPool;
	}

	def static init(int queueSize) {
		LOG.info("设置的线程池队列大小为:{}",queueSize);
		var workQueue = new ArrayBlockingQueue<Runnable>(queueSize <= 0 ? coreSize : queueSize) // 指定一种队列 （有界队列）
		threadPool = new ThreadPoolExecutor(
			coreSize,
			coreSize,
			60,
			TimeUnit.SECONDS,
			workQueue,
			new TaomuRunsPolicy() // 自定义拒绝策略
		)
		Runtime.runtime.addShutdownHook(new Thread() {
			override run() {
				LOG.info("关闭线程池");
				threadPool.shutdown();
			}
		})
	}
}
