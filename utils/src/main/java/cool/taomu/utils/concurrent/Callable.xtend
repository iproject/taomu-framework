package cool.taomu.utils.concurrent

import java.io.Serializable

interface Callable<V> extends java.util.concurrent.Callable<V>,Runnable, Serializable {
	
}