package cool.taomu.utils.concurrent

import java.io.Serializable

interface Future<V> extends java.util.concurrent.Future<V>, Runnable,Serializable {
	
}