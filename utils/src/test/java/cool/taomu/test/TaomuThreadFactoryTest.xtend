package cool.taomu.test

import cool.taomu.thread.TaomuThreadPoolFactory
import cool.taomu.utils.concurrent.Runnable

class MyTask implements Runnable {

	 int taskId;
	String taskName;
	
	new(int taskId, String taskName){
		this.taskId = taskId;
		this.taskName = taskName;
	}
	
	/**
	此处省略get/set方法
	**/

	override void run() {
		try {
			System.out.println("run taskId =" + this.taskId);
			Thread.sleep(5*1000);
			System.out.println("end taskId =" + this.taskId);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	override String toString(){
		return Integer.toString(this.taskId);
	}

}

class TaomuThreadFactoryTest {
	
	def static void main(String[] args)	{
		TaomuThreadPoolFactory.init(3)
		var a = TaomuThreadPoolFactory.getThreadPool;
		a.execute(new MyTask(1, "任务1"));
		a.execute(new MyTask(2, "任务2"));
		a.execute(new MyTask(3, "任务3"));
		a.execute(new MyTask(4, "任务4"));
		a.execute(new MyTask(5, "任务5"));
		a.execute(new MyTask(6, "任务6"));
		a.execute(new MyTask(7, "任务7"));
		a.execute(new MyTask(8, "任务8"));
		a.execute(new MyTask(9, "任务9"));
	}
}