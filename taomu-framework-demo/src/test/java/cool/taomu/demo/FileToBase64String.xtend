package cool.taomu.demo

import java.io.File
import java.io.FileInputStream
import org.apache.commons.codec.binary.Base64
import org.apache.commons.io.FileUtils

class FileToBase64String {
	def static void FileToBase64String(String inputPath, String outputPath) {
		var input = new FileInputStream(new File(inputPath));
		var bf = newByteArrayOfSize(input.available);
		input.read(bf)
		var base64 = Base64.encodeBase64(bf);
		FileUtils.writeByteArrayToFile(new File(outputPath),base64,true)
	}

	def static void base64ToFile(String inputPath, String outputPath) {
		var input = FileUtils.readFileToByteArray(new File(inputPath));
		var base64 = Base64.decodeBase64(input);
		FileUtils.writeByteArrayToFile(new File(outputPath), base64, true);
	}

	def static void main(String[] args) {
		//FileToBase64String("/home/rcmu/download/openlogic-openjdk-8u332-b09-linux-x64.tar.gz",
			//"/home/rcmu/download/a.txt");
		base64ToFile("/home/rcmu/download/a.txt", "/home/rcmu/download/b.tar.gz");
	}
}
