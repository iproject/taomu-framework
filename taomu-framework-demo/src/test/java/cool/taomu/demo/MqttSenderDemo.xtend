package cool.taomu.demo

import cool.taomu.framework.client.mqtt.Sender
import cool.taomu.framework.client.mqtt.SenderEntity
import io.netty.handler.codec.mqtt.MqttQoS

class MqttSenderDemo {
	def static void main(String[] args) {
		var sender = new SenderEntity();
		sender.topic = "topic_a2";
		sender.message = "Hello World2";
		sender.qos = MqttQoS.AT_LEAST_ONCE;
		sender.username = "admin";
		sender.password = "123456";
		var s = new Sender();
		s.send(sender);
	}
}
