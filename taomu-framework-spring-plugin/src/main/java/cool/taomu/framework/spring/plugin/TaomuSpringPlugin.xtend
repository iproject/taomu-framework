package cool.taomu.framework.spring.plugin

import cool.taomu.framework.utils.spi.ISpringPlugin
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.support.BeanDefinitionBuilder
import org.springframework.beans.factory.support.DefaultListableBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.support.GenericApplicationContext

class TaomuSpringPlugin implements ISpringPlugin {
	val LOG = LoggerFactory.getLogger(TaomuSpringPlugin)
	ApplicationContext applicationContext;

	new(GenericApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	override getBeanObject(Class<?> zlass) {
		LOG.info(zlass.name);
		this.register(zlass.name, zlass);
		return applicationContext.getBean(zlass.name);
	}

	def register(String key, Class<?> zlass) {
		var bean = BeanDefinitionBuilder.genericBeanDefinition(zlass);
		var config = applicationContext as ConfigurableApplicationContext;
		var registry = config.getBeanFactory() as DefaultListableBeanFactory
		registry.setAllowBeanDefinitionOverriding(true);
		if (registry.containsBeanDefinition(key)) {
			registry.removeBeanDefinition(key);
		}
		registry.registerBeanDefinition(key, bean.getRawBeanDefinition());
	}

}
