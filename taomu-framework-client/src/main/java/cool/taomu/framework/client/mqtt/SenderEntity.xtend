/***
/***
 * Copyright (c) 2022 murenchao
 * taomu framework is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.framework.client.mqtt

import cool.taomu.framework.configure.ConfigureManage
import cool.taomu.framework.crypto.ICrypto
import io.netty.handler.codec.mqtt.MqttQoS
import java.io.Serializable
import java.util.UUID
import org.apache.commons.lang3.SerializationUtils
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class SenderEntity implements Serializable {

	val config = ConfigureManage.loadConfig;
	String clientId;
	String ip = config.mqtt.hostname;
	MqttQoS qos = MqttQoS.AT_LEAST_ONCE;
	String topic;
	String username = config.mqtt.username.trim;
	String password = config.mqtt.password.trim;
	String message = "None";
	boolean retain = false;
	int timeout = 10;
	int port = config.mqtt.port;

	new() {
		this.clientId = UUID.randomUUID.toString();
	}

	new(String topic) {
		this.clientId = UUID.randomUUID.toString();
		this.topic = topic;
	}

	new(String clientId, String topic) {
		this.clientId = clientId;
		this.topic = topic;
	}

	def topic(String topic) {
		this.topic = topic;
		return this;
	}

	def clientId(String clientId) {
		this.clientId = clientId;
		return this;
	}

	def message(String message) {
		return this.message(message, null);
	}

	def message(String message, ICrypto crypto) {
		if (crypto !== null) {
			this.message = new String(crypto.setData(message.bytes).encode(), "UTF-8");
		} else {
			this.message = message;
		}
		return this;
	}

	def qos(MqttQoS qos) {
		this.qos = qos;
		return this;
	}

	def retain(boolean retain) {
		this.retain = retain;
		return this;
	}

	def ip(String broker) {
		this.ip = ip;
		return this;
	}

	def broker(String ip, int port) {
		this.ip = ip;
		this.port = port
		return this;
	}

	def port(int port) {
		this.port = port;
		return this;
	}

	def username(String username) {
		this.username = username;
		return this;
	}

	def password(String pwd) {
		this.password = pwd;
		return this;
	}

	override SenderEntity clone() {
		return SerializationUtils.<SenderEntity>clone(this);
	}
}
