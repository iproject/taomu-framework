package cool.taomu.framework.client.mqtt

import com.google.gson.Gson
import cool.taomu.framework.client.mqtt.annotation.Topic
import cool.taomu.framework.service.utils.CommonUtils
import cool.taomu.framework.utils.spi.ISpringPlugin
import io.netty.handler.codec.mqtt.MqttQoS
import java.util.ArrayList
import java.util.UUID
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.slf4j.LoggerFactory

class Subscriber {
	val LOG = LoggerFactory.getLogger(Subscriber)
	String username;
	String password;
	String ip = "0.0.0.0";
	int port = 1883;

	new(String username, String password) {
		this.username = username;
		this.password = password;
	}

	new(String ip, int port, String username, String password) {
		this.username = username;
		this.password = password;
		this.ip = ip;
		this.port = port;
	}

	def subscriber(Class<? extends MqttCallback> ... callbacks) {
		this.subscriber(null, callbacks);
	}

	def subscriber(ISpringPlugin spring, Class<? extends MqttCallback> ... callbacks) {
		callbacks.filterNull.forEach [
			CommonUtils.exec([
				var topic = it.getAnnotation(Topic);
				if (topic !== null) {
					LOG.info("Topic : " + new Gson().toJson(topic.value));
					val memoryPersistence = new MemoryPersistence();
					val mqttConnectOptions = new MqttConnectOptions();
					mqttConnectOptions.setCleanSession(topic.cleanSession);
					mqttConnectOptions.setUserName(username);
					mqttConnectOptions.setPassword(password.toCharArray());
					mqttConnectOptions.setConnectionTimeout(topic.timeout);
					mqttConnectOptions.setKeepAliveInterval(topic.keepAlive);
					var broker = String.format("%s://%s:%d", topic.isSsl ? "ssl" : "tcp", this.ip, this.port);
					var uuid = UUID.randomUUID.toString; // .replace("-", "");
					if (!topic.clientId.equals("uuid")) {
						uuid = topic.clientId;
					}
					val mqttClient = new MqttClient(broker, uuid, memoryPersistence);
					mqttClient.setCallback(mqttConnectOptions, it, spring);
					mqttClient.connect(mqttConnectOptions);

					var qoss = new ArrayList();
					qoss.addAll(topic.qos.map[return it.value]);
					for (var i = 0; qoss.size < topic.value.size && i < topic.value.size - qoss.size; i++) {
						qoss.add(MqttQoS.AT_MOST_ONCE.value);
					}
					mqttClient.subscribe(topic.value, qoss);
					Runtime.runtime.addShutdownHook(new Thread() {
						override run() {
							mqttClient.disconnect;
							memoryPersistence.close;
						}
					})
				}
			])
		]
		Runtime.runtime.addShutdownHook(new Thread() {
			override void run() {
				CommonUtils.shutdown();
			}
		})
	}
}
