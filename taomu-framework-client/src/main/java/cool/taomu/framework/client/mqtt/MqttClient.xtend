package cool.taomu.framework.client.mqtt

import cool.taomu.framework.utils.spi.ISpringPlugin
import org.eclipse.paho.client.mqttv3.MqttClientPersistence
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException

class MqttClient extends org.eclipse.paho.client.mqttv3.MqttClient {

	new(String serverURI, String clientId, MqttClientPersistence persistence) throws MqttException {
		super(serverURI, clientId, persistence)
	}

	def setCallback(MqttConnectOptions mqttConnectOptions, Class<? extends MqttCallback> callbackZlass) {
		this.setCallback(mqttConnectOptions, callbackZlass, null);
	}

	def setCallback(MqttConnectOptions mqttConnectOptions, Class<? extends MqttCallback> callbackZlass,
		ISpringPlugin springPlugin) {
		var MqttCallback cb = null;
		if (springPlugin !== null) {
			cb = callbackZlass.cast(springPlugin.getBeanObject(callbackZlass));
		} else {
			cb = callbackZlass.newInstance;
		}
		cb.client = this;
		cb.options = mqttConnectOptions;
		super.callback = cb;
	}
}
